package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.dto.Domain;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.ProjectRepository;
import ru.volnenko.se.repository.TaskRepository;

import java.util.List;

@Service
@Transactional
public class DomainService implements IDomainService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Domain getDomain() {
        List<Project> projects = projectRepository.findAll();
        List<Task> tasks = taskRepository.findAll();
        Domain domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        return domain;
    }

    @Override
    public void setDomain(final Domain domain) {
        if (domain.getProjects() != null && !domain.getProjects().isEmpty()) {
            final List<Project> projects = domain.getProjects();
            projectRepository.saveAll(projects);
        }

        if (domain.getTasks() != null && !domain.getTasks().isEmpty()) {
            final List<Task> tasks = domain.getTasks();
            taskRepository.saveAll(tasks);
        }
    }
}
