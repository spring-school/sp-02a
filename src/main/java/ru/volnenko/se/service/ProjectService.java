package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.List;

/**
 * @author Denis Volnenko
 */

@Service
@Transactional
public class ProjectService implements ru.volnenko.se.api.service.IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project getOne(String s) {
        return projectRepository.getOne(s);
    }

    @Override
    public Project createProject() {
        final Project project = new Project();
        return projectRepository.save(project);
    }

    @Override
    public <S extends Project> S save(S s) {
        return projectRepository.save(s);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void deleteById(String s) {
        projectRepository.deleteById(s);
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }


}
