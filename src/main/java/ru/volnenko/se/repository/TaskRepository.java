package ru.volnenko.se.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Override
    Task getOne(String s);

    @Override
    List<Task> findAll();

    @Override
    <S extends Task> S save(S s);

    @Override
    <S extends Task> List<S> saveAll(Iterable<S> iterable);

    @Override
    void deleteById(String s);

    @Override
    void deleteAll();

}
