package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface ITaskService {

    Task getOne(String s);

    List<Task> findAll();

    Task createTask();

    <S extends Task> S save(S s);

    void deleteById(String s);

    void deleteAll();

}
