package ru.volnenko.se.api.service;

import ru.volnenko.se.dto.Domain;

public interface IDomainService {

    Domain getDomain();

    void setDomain(Domain domain);

}
