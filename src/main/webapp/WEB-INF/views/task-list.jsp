<%@ page import="ru.volnenko.se.entity.Task" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>[TASKS]</title>
</head>
<body>

<h2>Task Manager</h2>

<table width="100%" cellspacing="10" cellpadding="10" border="1">
    <tr>
        <th colspan="7" align="center">
            TASKS
        </th>
    </tr>
    <tr>
        <th width="60" nowrap="nowrap" align="center">№</th>
        <th width="150" nowrap="nowrap" align="center">ID</th>
        <th width="150" nowrap="nowrap" align="center">NAME</th>
        <th width="150" nowrap="nowrap" align="center">DATE BEGIN</th>
        <th width="80" nowrap="nowrap" align="center">EDIT</th>
        <th width="80" nowrap="nowrap" align="center">REMOVE</th>
    </tr>

    <c:forEach var="task" items="${tasks}" varStatus="status">
        <tr>
            <td align="center" nowrap="nowrap">${status.index +1}."</td>
            <td align="left">${task.id}</td>
            <td align="left">${task.name}</td>
            <td align="left">${task.dateBegin}</td>
            <td align="center" nowrap="nowrap">
                <a href="/task-edit/${task.id}">EDIT</a>
            </td>
            <td align="center" nowrap="nowrap">
                <a href="/task-delete/${task.id}">REMOVE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td nowrap="nowrap">
            <form action="/task-create">
                <button>CREATE TASK</button>
            </form>
        </td>
        <td width="20" nowrap="nowrap"> </td>
        <td nowrap="nowrap">
            <form>
                <input type="hidden" name="time" value="<%=System.currentTimeMillis()%>">
                <button>REFRESH</button>
            </form>
        </td>
        <td width="100%"> </td>
    </tr>
</table>

</body>
</html>