<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
    <head>
        <title>Index page</title>
    </head>

    <body>
        <h1>WELCOME TO TASK MANAGER</h1>

        <h2><a href="/project-list">Projects</a></h2>
        <h2><a href="/task-list">Tasks</a></h2>
    </body>
</html>